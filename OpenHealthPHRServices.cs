﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ServiceModel.Description;
using PHEI.BLL;
using PHEI.DTO;
using caalhp.Core.Contracts;
using caalhp.IcePluginAdapters;

namespace OpenHealthPHRService
{
    [ServiceContract]
    public interface IPatientService
    {

        [OperationContract]
        void AddVitalSign(VitalSign vitalSign, string socialSecurity, string passKey);
        [OperationContract]
        void AddVitalSigns(VitalSign[] vitalSigns, DateTime Expires, string socialSecurity, string passKey);
        [OperationContract]
        void AddEvent(Event ev, string socialSecurity, string passKey);
        [OperationContract]
        void AddEvents(Event[] events, DateTime Expires, string socialSecurity, string passKey);
        [OperationContract]
        Event[] RetrieveAllEvents(string socialSecurity, string passkey);
        [OperationContract]
        VitalSign[] RetrieveAllVitalSigns(string socialSecurity, string passkey);
        [OperationContract]
        void UpdatePatient(Patient patientUpdateInfo, string socialSecurity, string passkey);
        [OperationContract]
        Patient CreateNewPatient(Patient dto);
        //[OperationContract]
        //Patient[] RetrieveAllPatients(string socialSecurity, string passkey);
        [OperationContract]
        void Test(BloodPressure bp);
        [OperationContract]
        Patient CreateNewPatientOnHealthCareUser(Patient dto, string socialSecurity, string passkey);
        [OperationContract]
        Patient[] RetrieveAllPatientsOnHealthCareProfessional(string socialSecurity, string passkey);
        [OperationContract]
        Patient[] RetrieveAllUnassignedPatients();
        [OperationContract]
        Patient[] RetrieveAllPatients(int HealthCareUserID);
        [OperationContract]
        HealthCareUser[] RetrieveAllHealthcareUsers();
        [OperationContract]
        HealthCareUser CreateNewHealthCareUser(HealthCareUser dto);
    }

    /// <summary>
    /// 
    /// </summary>
    public class PatientService : IPatientService
    {

        string DB_NAME = "vitalsigns.db";

        public void AddVitalSign(VitalSign vitalSign, string socialSecurity, string passKey)
        {
            var pc = new PatientController(DB_NAME);
            pc.AddVitalSign(vitalSign, socialSecurity, passKey);
            
        }

        public void AddVitalSigns(VitalSign[] vitalSigns, DateTime Expires, string socialSecurity, string passKey)
        {
            var pc = new PatientController(DB_NAME);
            pc.AddVitalSigns(vitalSigns, Expires, socialSecurity, passKey);
        }

        public void AddEvent(Event ev, string socialSecurity, string passKey)
        {
            var pc = new PatientController(DB_NAME);
            pc.AddEvent(ev, socialSecurity, passKey);
        }

        public void AddEvents(Event[] events, DateTime Expires, string socialSecurity, string passKey)
        {
            var pc = new PatientController(DB_NAME);
            pc.AddEvents(events, Expires, socialSecurity, passKey);
        }

        public Event[] RetrieveAllEvents(string socialSecurity, string passkey)
        {
            var pc = new PatientController(DB_NAME);
            return pc.RetrieveAllEvents(socialSecurity, passkey);
        }

        public VitalSign[] RetrieveAllVitalSigns(string socialSecurity, string passkey)
        {
            var pc = new PatientController(DB_NAME);
            return pc.RetrieveAllVitalSigns(socialSecurity, passkey);
        }

        public void UpdatePatient(Patient patientUpdateInfo, string socialSecurity, string passkey)
        {
            var pc = new PatientController(DB_NAME);
            pc.UpdatePatient(patientUpdateInfo, socialSecurity, passkey);
        }


        public Patient CreateNewPatient(Patient dto)
        {
            var pc = new PatientController(DB_NAME);
            return pc.CreateNewPatient(dto);
        }



        public void Test(BloodPressure bp)
        {
            throw new NotImplementedException();
        }


        public Patient CreateNewPatientOnHealthCareUser(Patient dto, string socialSecurity, string passkey)
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.CreateNewPatient(dto, socialSecurity, passkey);
        }

        public Patient[] RetrieveAllPatientsOnHealthCareProfessional(string socialSecurity, string passkey)
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.RetrieveAllPatients(socialSecurity, passkey);
        }

        public Patient[] RetrieveAllUnassignedPatients()
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.RetrieveAllUnassignedPatients();
        }

        public Patient[] RetrieveAllPatients(int HealthCareUserID)
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.RetrieveAllPatients(HealthCareUserID);
        }

        public HealthCareUser[] RetrieveAllHealthcareUsers()
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.RetrieveAllHealthcareUsers();
        }


        public HealthCareUser CreateNewHealthCareUser(HealthCareUser dto)
        {
            var hc = new HealthCareUserController(DB_NAME);
            return hc.CreateNewHealthCareUser(dto);
        }


    }

    public class OpenHealthHostingService
    {
        private ServiceHost host;
        /// <summary>
        /// The base adress where to reach this service (refactor to settings file)
        /// </summary>
        Uri baseAddress = new Uri("http://localhost:8083/VitalSignsService");

        /// <summary>
        /// Will start a new service host listing at the given baseadress
        /// </summary>
        public void DoHost()
        {
            // Create the ServiceHost.
            host = new ServiceHost(typeof(PatientService), baseAddress);
            //using (ServiceHost host = new ServiceHost(typeof(PatientService), baseAddress))
            {
                // Enable metadata publishing.
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                host.Description.Behaviors.Add(smb);

                // Open the ServiceHost to start listening for messages. Since
                // no endpoints are explicitly configured, the runtime will create
                // one endpoint per base address for each service contract implemented
                // by the service.
                host.Open();

                Console.WriteLine("The service is ready at {0}", baseAddress);
                Console.WriteLine("Press <Enter> to stop the service.");
            }
        }

        public void Close()
        {
            host.Close();
        }


    }
    /// <summary>
    /// The main service as a Console host app
    /// </summary>
    public class ConsoleHostingServiceImplementation : IServiceCAALHPContract
    {
        private static ServiceAdapter _adapter;
        private static IServiceCAALHPContract _implementation;
        private static OpenHealthHostingService p = null;

        /// <summary>
        /// Main function (starting point of application - if run from here)
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
                ConsoleHostingServiceImplementation c = new ConsoleHostingServiceImplementation();
                Task t = null;    
            try
                {

                    t = Task.Run(() =>
                    {
                        p = new OpenHealthHostingService();
                        p.DoHost();

                    });
                    t.Wait();
            }
            catch { }
            //t.Start();
            //Allows us to do other things ... such as 
            const string endpoint = "localhost";
            try
            {
                _implementation = (IServiceCAALHPContract)(c);
                _adapter = new ServiceAdapter(endpoint, _implementation);
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                Console.WriteLine(e.Message);
            }

            if (t!= null)
                t.Wait();
            //Console.ReadLine();

            // Close the ServiceHost.

            Console.WriteLine("Press <ENTER> to exit program.");
            Console.ReadLine();
            p.Close();
            

        }

       

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public string GetName()
        {
            return "ConsoleHostingServiceImplementation";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }
    }
}
